package com.epam.rd.khranovskyi.testing.controller;

import com.epam.rd.khranovskyi.dao.UserDAO;
import com.epam.rd.khranovskyi.dao.entity.Activity;
import com.epam.rd.khranovskyi.dao.entity.Language;
import com.epam.rd.khranovskyi.dao.entity.Role;
import com.epam.rd.khranovskyi.dao.entity.User;
import com.epam.rd.khranovskyi.service.UsersModifyService;
import com.epam.rd.khranovskyi.testing.AbstractBaseSpringTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

public class UserControllerIT extends AbstractBaseSpringTest {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UsersModifyService usersModifyService;

    @Test
    public void testInsertUserMethod_ThrowLayers() throws Exception {
        //Given
        User user = User.createUser("test@mail.com", "Test", "Testovich","321", Role.ADMIN, Language.RUSSIAN, new ArrayList<Activity>());
        //When
        usersModifyService.modifyUser(user, "0");
        //Then
        Assert.assertEquals(userDAO.findUserByMail("test@mail.com").toString(),user.toString());
    }

    @Test
    public void testDeleteUserMethod_ThrowLayers() throws Exception{
        //Given
        User user = User.createUser("test@mail.com", "Test", "Testovich","321", Role.ADMIN, Language.RUSSIAN, new ArrayList<Activity>());
        //When
        usersModifyService.modifyUser(user, "2");
        //Then
        Assert.assertNull(userDAO.findUserByMail("test@mail.com"));
    }
}
