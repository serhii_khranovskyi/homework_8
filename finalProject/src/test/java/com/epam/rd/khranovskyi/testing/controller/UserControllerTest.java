package com.epam.rd.khranovskyi.testing.controller;


import com.epam.rd.khranovskyi.dao.UserDAO;
import com.epam.rd.khranovskyi.testing.AbstractBaseSpringTest;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.ClientProtocolException;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.classic.methods.HttpGet;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.classic.methods.HttpUriRequest;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.http.HttpResponse;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.http.HttpStatus;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.shaded.org.apache.commons.lang.RandomStringUtils;

import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@TestPropertySource(properties = {"spring.config.location = classpath:application.yml"})
public class UserControllerTest extends AbstractBaseSpringTest {
    @Mock
    private UserDAO userDAO;

    @Test
    public void testAddClientFromForm_WithoutErrors() throws Exception {
        this.mockMvc
                .perform(get("/profile"))
                .andExpect(status().isOk());
    }

    @Test
    public void givenUserDoesNotExists_whenIsRetrieved_then404IsReceived()
            throws ClientProtocolException, IOException {

        String name = RandomStringUtils.randomAlphabetic(5);
        HttpUriRequest request = new HttpGet("http://localhost:8080/accounting_time/user" + name);

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        Assert.assertEquals(
                httpResponse.getCode(),
                (HttpStatus.SC_NOT_FOUND));
    }
}
