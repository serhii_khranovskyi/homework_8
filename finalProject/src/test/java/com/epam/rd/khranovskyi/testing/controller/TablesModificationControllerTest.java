package com.epam.rd.khranovskyi.testing.controller;

import com.epam.rd.khranovskyi.service.CategoryModifyService;
import com.epam.rd.khranovskyi.testing.AbstractBaseSpringTest;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.ClientProtocolException;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.classic.methods.HttpGet;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.classic.methods.HttpUriRequest;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.http.HttpResponse;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.http.HttpStatus;
import com.sun.deploy.net.HttpRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;
import org.testcontainers.shaded.org.apache.commons.lang.RandomStringUtils;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TablesModificationControllerTest  extends AbstractBaseSpringTest {

    @Test
    public void changingLanguage() throws Exception{
        this.mockMvc
                .perform(get("/language"))
                .andExpect(status().isOk());
    }

    @Test
    public void givenTableForModifyingDoesNotExist_whenIsRetrieved_then404IsReceived()
            throws ClientProtocolException, IOException {

        String name = RandomStringUtils.randomAlphabetic( 10 );
        HttpUriRequest request = new HttpGet( "http://localhost:8080/accounting_time/modify" + name );

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );

        Assert.assertEquals(
                httpResponse.getCode(),
                (HttpStatus.SC_NOT_FOUND));
    }
}
