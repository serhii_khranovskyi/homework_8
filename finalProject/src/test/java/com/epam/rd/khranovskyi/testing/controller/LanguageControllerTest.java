package com.epam.rd.khranovskyi.testing.controller;

import com.epam.rd.khranovskyi.testing.AbstractBaseSpringTest;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.ClientProtocolException;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.classic.methods.HttpGet;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.classic.methods.HttpUriRequest;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.http.HttpResponse;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.http.HttpStatus;
import org.junit.Assert;
import org.junit.Test;
import org.testcontainers.shaded.org.apache.commons.lang.RandomStringUtils;

import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LanguageControllerTest  extends AbstractBaseSpringTest {

    @Test
    public void changingLanguage() throws Exception{
        this.mockMvc
                .perform(get("/language"))
                .andExpect(status().isOk());
    }
    @Test
    public void givenLanguageDoesNotExists_whenIsRetrieved_then404IsReceived()
            throws ClientProtocolException, IOException {

        String name = RandomStringUtils.randomAlphabetic( 2 );
        HttpUriRequest request = new HttpGet( "http://localhost:8080/accounting_time/language/" + name );

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );

        Assert.assertEquals(
                httpResponse.getCode(),
                (HttpStatus.SC_NOT_FOUND));
    }
}
