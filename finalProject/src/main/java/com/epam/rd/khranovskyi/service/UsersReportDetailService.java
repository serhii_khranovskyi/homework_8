package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.ActivityDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.bean.UserActivityBean;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.List;

@Service
public class UsersReportDetailService {
    private static final Logger LOG = Logger.getLogger(UsersReportService.class);

    @Autowired
    private ActivityDAO activityDAO;

    public String execute(Model model, WebRequest request) throws IOException {
        LOG.debug("Commands starts");

        String mail = (String) request.getParameter("mail");
        //get the detail information of user`s activities
        List<UserActivityBean> activities = activityDAO.getUserActivities(mail);
        request.setAttribute("activities", activities, RequestAttributes.SCOPE_REQUEST);
        LOG.debug("Commands finished");

        return Path.PAGE__USER_REPORT_DETAIL;
    }
}
