package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.ActivityDAO;
import com.epam.rd.khranovskyi.dao.LocalizationDAO;
import com.epam.rd.khranovskyi.dao.Path;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.List;

@Service
public class ActivitiesLocalizationModifyService {
    private static final Logger LOG = Logger.getLogger(ActivitiesLocalizationModifyService.class);

    @Autowired
    private ActivityDAO activityDAO;
    @Autowired
    private LocalizationDAO localizationDAO;

    public String execute(Model model, WebRequest request) throws IOException {
        String combobox = request.getParameter("combobox_localization");
        LOG.trace("Request parameter: combobox_localization -->" + combobox);

        String activity_name = request.getParameter("activity_name_localization");
        LOG.trace("Request parameter: activity_name --> " + activity_name);

        String translation = request.getParameter("translation");
        LOG.trace("Request parameter: translation --> " + translation);

        String radio = request.getParameter("language");
        LOG.trace("Request parameter: radio--> " + radio);


        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        if (activity_name == null || translation == null) {
            errorMessage = "Parameters cannot be empty";
            request.setAttribute("errorMessage", errorMessage, RequestAttributes.SCOPE_REQUEST);
            LOG.error("errorMessage --> " + errorMessage);
            return forward;
        }
        //insert the translation of activity
        if (combobox.equals("0")) {
            activityDAO.insertActivityTranslation(activity_name, translation, "English");
        }
        //update  the translation of activity
        if (combobox.equals("1")) {
            LOG.trace("Update activity translation");
            activityDAO.updateActivityTranslation(activity_name, translation, "English");

        }
        //delete the translation of activity
        if (combobox.equals("2")) {
            activityDAO.deleteActivityTranslation(translation);
        }

        //get languages for radio buttons
        List<String> languagesList = localizationDAO.getLanguagesList();
        LOG.trace("Found in DB: list of languages -->" + languagesList);
        request.setAttribute("languagesList", languagesList, RequestAttributes.SCOPE_REQUEST);


        return "redirect:" + Path.COMMAND__ACTIVITIES;
    }
}
