package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.UserDAO;
import com.epam.rd.khranovskyi.dao.entity.Language;
import com.epam.rd.khranovskyi.dao.entity.Role;
import com.epam.rd.khranovskyi.dao.entity.User;
import com.epam.rd.khranovskyi.web.exception.ClientException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.ArrayList;

@Service
public class RegistrationService {

    private static final Logger LOGGER = Logger.getLogger(LoginService.class);

    @Autowired
    private UserDAO userDAO;

    public String execute(Model model, WebRequest webRequest) throws IOException {
        LOGGER.debug("Command starts");

        String locale = (String) model.getAttribute("defaultLocale");
        LOGGER.trace("Get locale" + locale);
        // register a new user from the request
        String first_name = webRequest.getParameter("first_name");
        LOGGER.trace("Request parameter: first_name --> " + first_name);


        String last_name = webRequest.getParameter("last_name");
        LOGGER.trace("Request parameter: last_name --> " + last_name);

        String mail = webRequest.getParameter("mail");
        LOGGER.trace("Request parameter: mail --> " + mail);

        String password = webRequest.getParameter("password");
        LOGGER.trace("Request parameter: password --> " + password);

        String rewritePassword = webRequest.getParameter("rewrite_password");
        LOGGER.trace("Request parameter: password --> " + rewritePassword);

        if (first_name == null || last_name == null ||
                mail == null || password == null ||
                !password.equals(rewritePassword) || password.isEmpty()) {
            LOGGER.trace("throw ClientException");
            throw new ClientException("Login/password cannot be empty");
        }

        LOGGER.info("Check the user");
        boolean isNewUser = userDAO.checkUser(mail);
        LOGGER.info("isNewUser = " + isNewUser);

        if (!isNewUser) {
            LOGGER.trace("throw ClientException");
            throw new ClientException("This mail is registered");
        } else {
            Language language = Language.RUSSIAN;
            LOGGER.trace("NewUser firstName --> " + first_name + " LastName--> " + last_name + " mail -->" + mail + " Password-->" + password + " Locale-->" + locale);
            if ("en".equals(locale)) {
                language = Language.ENGLISH;
            }
            //Create new user
            User user = User.createUser(mail, first_name, last_name, password, Role.USER, language, new ArrayList<>());
            //insert user
            userDAO.insertUser(user);
        }

        LOGGER.debug("Service finished");
        return Path.PAGE__LOGIN;
    }
}
