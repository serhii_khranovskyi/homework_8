package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.ActivityDAO;
import com.epam.rd.khranovskyi.dao.LocalizationDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.entity.Activity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class ListOfActivitiesService {
    private static final Logger LOG = Logger.getLogger(ListOfActivitiesService.class);

    @Autowired
    private ActivityDAO activityDAO;
    @Autowired
    private LocalizationDAO localizationDAO;

    private static class CompareById implements Comparator<Activity>, Serializable {
        private static final long serialVersionUID = -1573481565177573283L;

        public int compare(Activity bean1, Activity bean2) {
            if (bean1.getId() > bean2.getId())
                return 1;
            else return -1;
        }
    }

    private static Comparator<Activity> compareById = new ListOfActivitiesService.CompareById();

    public String execute(Model model, WebRequest request) throws IOException {

        String locale = (String) model.getAttribute("defaultLocale");

        LOG.trace("Locale -->" + locale);
        //Get the list of activities
        List<Activity> activityList = activityDAO.getActivities();
        LOG.trace("Found in DB: activityList --> " + activityList);

        Collections.sort(activityList, compareById);

        //Get list of languages
        List<String> languagesList = localizationDAO.getLanguagesList();
        LOG.trace("Found in DB: list of languages -->" + languagesList);

        //set activity`s list to the request
        request.setAttribute("activityList", activityList, RequestAttributes.SCOPE_REQUEST);
        LOG.trace("Set the request attribute: activityList --> " + activityList);
        //set language`s list to the request
        request.setAttribute("languagesList", languagesList, RequestAttributes.SCOPE_REQUEST);
        LOG.trace("Set the request attribute: languagesList --> " + languagesList);
        LOG.debug("Commands finished");
        return Path.PAGE__ACTIVITIES;
    }

}
