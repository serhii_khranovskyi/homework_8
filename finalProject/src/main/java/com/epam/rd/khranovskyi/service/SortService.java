package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.ActivityDAO;
import com.epam.rd.khranovskyi.dao.LocalizationDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.bean.UserActivityBean;
import com.epam.rd.khranovskyi.dao.entity.Activity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.List;

@Service
public class SortService {
    private static final Logger LOG = Logger.getLogger(SortService.class);

    @Autowired
    private ActivityDAO activityDAO;

    @Autowired
    private LocalizationDAO localizationDAO;

    private static final String SQL__SORT_ACTIVITY_BY_NAME =
            "select id_activities, activity_name, category_name " +
                    "                    from activities join categories on categories_id_categories = id_categories " +
                    "                    order by activity_name ASC;";

    private static final String SQL__SORT_ACTIVITY_BY_NAME_TRANSLATION =
            "select id_activities, activities_localization.translation, categories_localization.translation " +
                    "                                       from activities join categories on activities.categories_id_categories = id_categories " +
                    "                                       join activities_localization on activities_localization.activities_id_activities = id_activities " +
                    "                                       join categories_localization on categories_localization.categories_id_categories = id_categories " +
                    "                                       join language on activities_localization.language_id_language = id_language " +
                    "                                       where name= ? " +
                    "                                       order by activities_localization.translation ASC;";

    private static final String SQL_SORT_ACTIVITY_BY_CATEGORY =
            "select id_activities, activity_name, category_name from activities " +
                    "   join categories on categories_id_categories = id_categories order by  category_name ASC;";

    private static final String SQL_SORT_ACTIVITY_BY_CATEGORY_TRANSLATION =
            "select id_activities, activities_localization.translation, categories_localization.translation " +
                    "                                       from activities join categories on activities.categories_id_categories = id_categories " +
                    "                                       join activities_localization on activities_localization.activities_id_activities = id_activities " +
                    "                                       join categories_localization on categories_localization.categories_id_categories = id_categories " +
                    "                                       join language on activities_localization.language_id_language = id_language " +
                    "                                       where name=? " +
                    "                                       order by categories_localization.translation ASC;";


    public String execute(Model model, WebRequest request) throws IOException {

        String language = (String) model.getAttribute("defaultLocale");
        LOG.trace("Language -->" + language);

        List<UserActivityBean> userActivityBeans = null;

        String sortActivityByName = request.getParameter("sortActivityByName");
        LOG.trace("SortCommand Parameter--> " + sortActivityByName);
        String sortActivityByCategory = request.getParameter("sortActivityByCategory");
        LOG.trace("SortCommandCategory Parameter--> " + sortActivityByCategory);
        String sortActivityByAmount = request.getParameter("sortActivityByAmount");
        LOG.trace("SortCommandByAmount Parameter--> " + sortActivityByAmount);

        List<Activity> activityList = null;
        //Sort in English
        if (language.equals("en")) {
            language = "English";
            //Sort by name
            if (sortActivityByName != null && !sortActivityByName.isEmpty()) {
                activityList = activityDAO.sortActivity(language, SQL__SORT_ACTIVITY_BY_NAME_TRANSLATION);
                //Sort by category
            } else if (sortActivityByCategory != null && !sortActivityByCategory.isEmpty()) {
                activityList = activityDAO.sortActivity(language, SQL_SORT_ACTIVITY_BY_CATEGORY_TRANSLATION);
            }//Sort by amount of users
            else if (sortActivityByAmount != null && !sortActivityByAmount.isEmpty()) {
                activityList = activityDAO.sortActivityByAmountOfUsers(language);
            }
            //Sort
        } else if (language.equals("ru")) {
            //Sort by name
            if (sortActivityByName != null && !sortActivityByName.isEmpty()) {
                activityList = activityDAO.sortActivity(SQL__SORT_ACTIVITY_BY_NAME);
                //Sort by category
            } else if (sortActivityByCategory != null && !sortActivityByCategory.isEmpty()) {
                activityList = activityDAO.sortActivity(SQL_SORT_ACTIVITY_BY_CATEGORY);
                //Sort by amount of users
            } else if (sortActivityByAmount != null && !sortActivityByAmount.isEmpty()) {
                activityList = activityDAO.sortActivityByAmountOfUsers();
            }
        }
        //get languages for radio buttons
        List<String> languagesList = localizationDAO.getLanguagesList();
        LOG.trace("Found in DB: list of languages -->" + languagesList);
        request.setAttribute("languagesList", languagesList, RequestAttributes.SCOPE_REQUEST);
        // set the sorted list to the request
        request.setAttribute("activityList", activityList, RequestAttributes.SCOPE_REQUEST);
        return Path.PAGE__ACTIVITIES;
    }
}
