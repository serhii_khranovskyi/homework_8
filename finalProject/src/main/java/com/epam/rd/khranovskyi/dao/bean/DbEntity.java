package com.epam.rd.khranovskyi.dao.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class DbEntity {
    @Setter
    @Getter
    public String dbDriver;

    @Setter
    @Getter
    private String dbURL;

    @Setter
    @Getter
    private String dbName;

    @Setter
    @Getter
    private String dbUsername;

    @Setter
    @Getter
    private String dbPassword;
}
