package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.ActivityDAO;
import com.epam.rd.khranovskyi.dao.CategoryDAO;
import com.epam.rd.khranovskyi.dao.LocalizationDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.bean.CategoryBean;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.List;

@Service
public class ListOfCategoriesService {

    private static final Logger LOG = Logger.getLogger(ListOfCategoriesService.class);

    @Autowired
    private CategoryDAO categoryDAO;
    @Autowired
    private LocalizationDAO localizationDAO;

    public String execute(Model model, WebRequest request) throws IOException {

        LOG.trace("Get https session");
        String locale = (String) model.getAttribute("defaultLocale");

        LOG.trace("Locale -->" + locale);
        //Get list of categories
        List<CategoryBean> categoryList = categoryDAO.getCategories();
        LOG.trace("Found in DB: categoryList --> " + categoryList);


        //get list of languages
        List<String> languagesList = localizationDAO.getLanguagesList();
        LOG.trace("Found in DB: list of languages -->" + languagesList);
        // put categories and languages beans lists to requests
        request.setAttribute("categoryList", categoryList, RequestAttributes.SCOPE_REQUEST);
        LOG.trace("Set the request attribute: categoryList --> " + categoryList);
        request.setAttribute("languagesList", languagesList, RequestAttributes.SCOPE_REQUEST);
        LOG.trace("Set the request attribute: languagesList --> " + languagesList);
        LOG.debug("Commands finished");
        return Path.PAGE__CATEGORIES;
    }
}
