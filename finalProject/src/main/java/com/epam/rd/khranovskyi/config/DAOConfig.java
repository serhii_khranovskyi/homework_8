package com.epam.rd.khranovskyi.config;

import com.epam.rd.khranovskyi.dao.bean.DbEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource("classpath:data.properties")
public class DAOConfig {
    @Value("${driver}")
    public String dbDriver;

    @Value("${url}")
    private String dbURL;


    @Value("${dbname}")
    private String dbName;


    @Value("${user}")
    private String dbUsername;


    @Value("${password}")
    private String dbPassword;

    @Bean
    public DbEntity DbEntity() {
        DbEntity entity = new DbEntity();
        entity.setDbDriver(dbDriver);
        entity.setDbURL(dbURL);
        entity.setDbName(dbName);
        entity.setDbUsername(dbUsername);
        entity.setDbPassword(dbPassword);
        return entity;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
