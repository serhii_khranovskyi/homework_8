package com.epam.rd.khranovskyi.dao;

import com.epam.rd.khranovskyi.dao.bean.UserActivityBean;
import com.epam.rd.khranovskyi.dao.bean.UserBean;
import com.epam.rd.khranovskyi.dao.entity.Activity;
import com.epam.rd.khranovskyi.dao.entity.Category;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class ActivityDAO {
    private static final Logger LOGGER = Logger.getLogger(ActivityDAO.class);

    private static final String SQL__ACTIVITY_INSERT_LOCALIZATION =
            "INSERT activities_localization (translation,activities_id_activities,language_id_language) values(?, ?, ?);";

    private static final String SQL__ACTIVITY_UPDATE_LOCALIZATION =
            "update activities_localization set translation  = ?, language_id_language = ? where activities_id_activities = ?; ";

    private static final String SQL__INSERT_ACTIVITY =
            "INSERT activities (categories_id_categories, activity_name)  " +
                    "values (?, ?);";

    private static final String SQL__UPDATE_ACTIVITY_BY_ID =
            "update wb.activities set activity_name  = ?, categories_id_categories = ? where id_activities = ?; ";

    private static final String SQL__GET_CATEGORY_ID =
            "select id_categories from categories where category_name = ?;";

    private static final String SQL__GET_ACTIVITY_ID =
            "select id_activities from activities where activity_name = ?;";

    private static final String SQL__GET_LANGUAGE_ID =
            "select id_language from language where name = ?;";


    private static final String SQL__DELETE_ACTIVITY_BY_ID =
            "DELETE FROM activities WHERE  id_activities= ?;";


    private static final String SQL_SORT_ACTIVITY_BY_COUNT_OF_USERS =
            "select id_activities, activity_name, category_name, COUNT(users.id_users) AS UserCount " +
                    "                    from users, activities, activities_users, categories " +
                    "                    where id_activities = activities_id_activities " +
                    "                    AND users_id_users = id_users " +
                    "                    AND categories_id_categories = id_categories group by activities_id_activities;";

    private static final String SQL_SORT_ACTIVITY_BY_COUNT_OF_USERS_TRANSLATION =
            "select id_activities, activities_localization.translation, categories_localization.translation, COUNT(users.id_users) AS UserCount \n" +
                    "                                      from users, activities, activities_users, categories, activities_localization, categories_localization, language\n" +
                    "                                    where id_activities = activities_users.activities_id_activities \n" +
                    "                                       AND activities_users.users_id_users = id_users \n" +
                    "                                       AND activities.categories_id_categories = id_categories\n" +
                    "                                       AND activities_localization.activities_id_activities = id_activities\n" +
                    "                                       AND categories_localization.categories_id_categories = id_categories \n" +
                    "                                       and language.id_language = activities_localization.language_id_language\n" +
                    "                                       and name = ? group by activities_users.activities_id_activities;";


    private static final String SQL__GET_ACTIVITIES1 = "SELECT id_activities, category_name, activity_name, activities_localization.translation " +
            "            FROM activities " +
            "            join categories on activities.categories_id_categories = id_categories " +
            "            left join activities_localization on activities_localization.activities_id_activities = id_activities;";

    private static final String SQL__GET_ACTIVITIES_BY_CATEGORY_ID =
            "select id_activities, activity_name " +
                    "from activities " +
                    "where categories_id_categories = ? " +
                    "and id_activities not in (select activities_id_activities from activities_users where users_id_users = ?);";

    private static final String SQL__GET_ACTIVITIES_TRANSLATION_BY_CATEGORY_ID =
            "select activities_id_activities, translation " +
                    "                    from activities inner join activities_localization on activities_id_activities = id_activities " +
                    "                    inner join language on language_id_language = id_language " +
                    "                    where categories_id_categories = ? and name = ? " +
                    "                    and id_activities not in (select activities_id_activities from activities_users where users_id_users = ?);";


    private static final String SQL__DELETE_ACTIVITY_TRANSLATION = "DELETE FROM activities_localization WHERE  translation= ?;";

    private static final String SQL_GET_AMOUNT_OF_USER_ACTIVITIES =
            "select count(activity_name) as count from activities, activities_users where activities_id_activities = id_activities and users_id_users = ? and status = 2; ";
    private static final String SQL__GET_USER_ACTIVITIES = "select id_activities, activity_name, beginning, ending from activities, activities_users where activities_id_activities = id_activities and users_id_users = ? and status = 2 limit ? , ?;";

    private static final String SQL__GET_USER_ACTIVITIES_TRANSLATION = "select id_activities, translation, beginning, ending from activities, activities_localization, activities_users, language where activities_users.activities_id_activities = id_activities and activities_localization.activities_id_activities = id_activities and activities_localization.language_id_language = id_language and users_id_users = ? and status = 2 and name= ? limit ? , ?;";

    private static final String SQL__GET_USER_ACTIVITIES_HISTORY_TRANSLATION =
            "select id_activities_userscol, translation, beginning, ending \n" +
                    "                   from activities, activities_localization, activities_users, language \n" +
                    "                   where activities_users.activities_id_activities = id_activities \n" +
                    "                   and activities_localization.activities_id_activities = id_activities \n" +
                    "                   and activities_localization.language_id_language = id_language\n" +
                    "                   and activities_users.users_id_users = ? and activities_users.activities_id_activities=? and name= ? " +
                    "                   order by id_activities_userscol DESC;";

    private static final String SQL__GET_USER_ACTIVITIES_HISTORY = "select id_activities_userscol, activity_name, beginning, ending from activities_users join activities on id_activities = activities_id_activities where activities_id_activities = ? and  users_id_users = ? order by id_activities_userscol DESC;";


    @Autowired
    private DBManager dbManager;

    /**
     * Get list of information from the activities_users table
     *
     * @param sql
     * @return
     */
    public List<UserBean> getUserBeans(String sql) {
        List<UserBean> userBeanList = new ArrayList<UserBean>();
        Statement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            UserBeanMapper mapper = new UserBeanMapper();
            while (rs.next())
                userBeanList.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return userBeanList;
    }

    /**
     * Get list of translated fields from the activities_users table
     *
     * @param sql
     * @param language
     * @return
     */
    public List<UserBean> getUserBeans(String sql, String language) {
        List<UserBean> userBeanList = new ArrayList<UserBean>();
        Statement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            UserBeanMapper mapper = new UserBeanMapper();
            while (rs.next())
                userBeanList.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return userBeanList;
    }

    /**
     * get list of activities` times
     *
     * @param mail
     * @return
     */
    public ArrayList<UserActivityBean> getUserActivities(String mail) {
        ArrayList<UserActivityBean> activityList = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            ActivityDAO.UsersActivity mapper = new ActivityDAO.UsersActivity();
            pstmt = con.prepareStatement("select activity_name, sum(ending - beginning)\n" +
                    " from activities \n" +
                    " join activities_users on id_activities = activities_id_activities \n" +
                    " join users on id_users = users_id_users\n" +
                    " where mail = ?" +
                    " group by activity_name;");
            pstmt.setString(1, mail);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                activityList.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityList;
    }

    /**
     * sort by amount of users
     *
     * @return
     */
    public ArrayList<Activity> sortActivityByAmountOfUsers() {
        ArrayList<Activity> activityList = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            ActivityDAO.ActivityUserMapper mapper = new ActivityDAO.ActivityUserMapper();
            pstmt = con.prepareStatement(SQL_SORT_ACTIVITY_BY_COUNT_OF_USERS);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                activityList.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityList;
    }

    /**
     * sort the translated information by amount of users
     *
     * @param language
     * @return
     */
    public ArrayList<Activity> sortActivityByAmountOfUsers(String language) {
        ArrayList<Activity> activityList = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            ActivityDAO.ActivityUserMapperTranslation mapper = new ActivityDAO.ActivityUserMapperTranslation();
            pstmt = con.prepareStatement(SQL_SORT_ACTIVITY_BY_COUNT_OF_USERS_TRANSLATION);
            pstmt.setString(1, language);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                activityList.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.trace("Error-->" + ex);
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityList;
    }

    /**
     * sort activities by category or by name
     *
     * @param sql
     * @return
     */
    public ArrayList<Activity> sortActivity(String sql) {
        ArrayList<Activity> activityList = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            ActivityDAO.ActivityMapper mapper = new ActivityDAO.ActivityMapper();
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                activityList.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityList;
    }

    /**
     * sort the translation of categories or names
     *
     * @param language
     * @param sql
     * @return
     */
    public ArrayList<Activity> sortActivity(String language, String sql) {
        ArrayList<Activity> activityList = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            ActivityDAO.ActivityMapperTranslation mapper = new ActivityDAO.ActivityMapperTranslation();
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, language);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                activityList.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityList;
    }

    /**
     * get list of activities
     *
     * @return
     */
    public ArrayList<Activity> getActivities() {
        ArrayList<Activity> activityArrayList = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            ActivityDAO.ActivityMapper2 mapper = new ActivityDAO.ActivityMapper2();
            pstmt = con.prepareStatement(SQL__GET_ACTIVITIES1);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                activityArrayList.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.error("Error in method getActivities. Message->" + ex);
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityArrayList;
    }

    /**
     * get the list of the activities of user
     *
     * @param userId
     * @param categotyId
     * @return
     */
    public ArrayList<Activity> getActivities(Long userId, String categotyId) {
        ArrayList<Activity> activityArrayList = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;

        try {
            con = dbManager.getConnection();
            ActivityDAO.ActivityCategoryMapper mapper = new ActivityDAO.ActivityCategoryMapper();
            pstmt = con.prepareStatement(SQL__GET_ACTIVITIES_BY_CATEGORY_ID);
            pstmt.setLong(1, Long.parseLong(categotyId));
            pstmt.setLong(2, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                activityArrayList.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.error("Error in method getActivities. Message->" + ex);
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityArrayList;
    }

    /**
     * get the list of activities translation
     *
     * @param userId
     * @param categotyId
     * @param language
     * @return
     */
    public ArrayList<Activity> getActivitiesTranslation(Long userId, String categotyId, String language) {
        ArrayList<Activity> activityArrayList = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;

        try {
            LOGGER.trace("Get activity starts");
            con = dbManager.getConnection();
            ActivityDAO.ActivityCategoryMapper mapper = new ActivityDAO.ActivityCategoryMapper();
            pstmt = con.prepareStatement(SQL__GET_ACTIVITIES_TRANSLATION_BY_CATEGORY_ID);
            pstmt.setLong(1, Long.parseLong(categotyId));
            pstmt.setString(2, language);
            pstmt.setLong(3, userId);
            LOGGER.trace("Set parameters");
            rs = pstmt.executeQuery();
            LOGGER.trace("Execute");
            while (rs.next()) {
                activityArrayList.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.error("Error in method getActivities. Message->" + ex);
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityArrayList;
    }

    /**
     * insert activity in activities table
     *
     * @param activity
     * @return
     */
    public int insertActivityWithoutTranslation(Activity activity) {

        Connection con = null;
        int activityId = 0;
        try {
            con = dbManager.getConnection();

            PreparedStatement getCategoryId = con.prepareStatement(SQL__GET_CATEGORY_ID);
            getCategoryId.setString(1, activity.getCategory().getName());
            ResultSet rs = getCategoryId.executeQuery();
            rs.next();
            int categoriesId = rs.getInt(Fields.CATEGORIES__ID_CATEGORIES);
            getCategoryId.close();
            rs.close();
            PreparedStatement insertActivity = con.prepareStatement(SQL__INSERT_ACTIVITY, Statement.RETURN_GENERATED_KEYS);
            insertActivity.setInt(1, categoriesId);
            insertActivity.setString(2, activity.getName());
            insertActivity.executeUpdate();
            ResultSet resultSet = insertActivity.getGeneratedKeys();
            if (resultSet.next()) {
                activityId = resultSet.getInt(1);
            }
            insertActivity.close();
            resultSet.close();

        } catch (SQLException ex) {
            LOGGER.error(ex);
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();

        } finally {
            dbManager.commitAndClose(con);
        }
        return activityId;
    }

    /**
     * insert activities translation into the activities_localization
     *
     * @param activity
     * @param translation
     * @param language
     */
    public void insertActivityTranslation(String activity, String translation, String language) {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();

            int activityId = getActivityId(activity, con);
            int languageId = getLanguageId(language, con);
            PreparedStatement updateLocale = con.prepareStatement(SQL__ACTIVITY_INSERT_LOCALIZATION);
            updateLocale.setString(1, translation);
            updateLocale.setInt(2, activityId);
            updateLocale.setInt(3, languageId);

            updateLocale.executeUpdate();

        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.trace("Error -->" + ex);
        } finally {
            dbManager.commitAndClose(con);
        }
    }

    /**
     * update the activities translation
     *
     * @param activity
     * @param translation
     * @param language
     */
    public void updateActivityTranslation(String activity, String translation, String language) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();

            int activityId = getActivityId(activity, con);
            int languageId = getLanguageId(language, con);
            PreparedStatement updateLocale = con.prepareStatement(SQL__ACTIVITY_UPDATE_LOCALIZATION);
            updateLocale.setString(1, translation);
            updateLocale.setInt(2, languageId);
            updateLocale.setInt(3, activityId);

            updateLocale.executeUpdate();

        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
            LOGGER.trace("Error -->" + ex);
        } finally {
            dbManager.commitAndClose(con);
        }
    }

    /**
     * get the primary key of activity
     *
     * @param activityName
     * @param con
     * @return
     * @throws SQLException
     */
    public int getActivityId(String activityName, Connection con) throws SQLException {
        PreparedStatement getActivityId = null;
        ResultSet resultSet = null;
        int activityId = 0;
        try {
            getActivityId = con.prepareStatement(SQL__GET_ACTIVITY_ID);
            getActivityId.setString(1, activityName);
            resultSet = getActivityId.executeQuery();
            resultSet.next();
            activityId = resultSet.getInt(Fields.ACTIVITY_ID);
        } catch (SQLException ex) {

        } finally {
            getActivityId.close();
            resultSet.close();
        }
        return activityId;
    }

    /**
     * get language primary key
     *
     * @param language
     * @param con
     * @return
     * @throws SQLException
     */
    public int getLanguageId(String language, Connection con) throws SQLException {
        PreparedStatement getLanguageId = null;
        ResultSet resultSet = null;
        int languageId = 0;
        try {
            getLanguageId = con.prepareStatement(SQL__GET_LANGUAGE_ID);
            getLanguageId.setString(1, language);
            resultSet = getLanguageId.executeQuery();
            resultSet.next();
            languageId = resultSet.getInt("id_language");
        } catch (SQLException ex) {

        } finally {
            getLanguageId.close();
            resultSet.close();
        }
        return languageId;
    }

    /**
     * overloaded activity update method
     *
     * @param activity
     */
    public void updateActivity(Activity activity) {
        Connection con = null;
        try {
            con = dbManager.getConnection();
            updateActivity(con, activity);
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
    }

    /**
     * overloaded activity update method
     *
     * @param con
     * @param activity
     * @throws SQLException
     */
    public void updateActivity(Connection con, Activity activity) throws SQLException {
        PreparedStatement getCategoryId = con.prepareStatement(SQL__GET_CATEGORY_ID);
        getCategoryId.setString(1, activity.getCategory().getName());
        ResultSet rs = getCategoryId.executeQuery();
        rs.next();
        int categoriesId = rs.getInt(Fields.CATEGORIES__ID_CATEGORIES);
        getCategoryId.close();
        PreparedStatement pstmt = con.prepareStatement(SQL__UPDATE_ACTIVITY_BY_ID);
        int k = 1;
        pstmt.setString(k++, activity.getName());
        pstmt.setLong(k++, categoriesId);
        pstmt.setLong(k++, activity.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * dalete activity by id
     *
     * @param activity
     */
    public void deleteActivity(Activity activity) {
        PreparedStatement pstmt = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL__DELETE_ACTIVITY_BY_ID);
            pstmt.setLong(1, activity.getId());
            pstmt.execute();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
    }

    /**
     * delete the translation of activity by translation
     *
     * @param activityTranslation
     */
    public void deleteActivityTranslation(String activityTranslation) {
        PreparedStatement pstmt = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL__DELETE_ACTIVITY_TRANSLATION);
            pstmt.setString(1, activityTranslation);
            pstmt.execute();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
    }

    /**
     * get the amount of activities` records in activities_users
     *
     * @param userId
     * @return
     */
    public int getAmountOfUserActivities(Long userId) {
        PreparedStatement getAmountOfUserActivities = null;
        ResultSet resultSet = null;
        Connection con = null;
        int activitiesAmount = 0;
        try {
            con = dbManager.getConnection();
            getAmountOfUserActivities = con.prepareStatement(SQL_GET_AMOUNT_OF_USER_ACTIVITIES);
            getAmountOfUserActivities.setLong(1, userId);
            resultSet = getAmountOfUserActivities.executeQuery();
            resultSet.next();
            activitiesAmount = resultSet.getInt("count");
        } catch (SQLException ex) {
            LOGGER.trace("Error in getAmountUserActivities method-->" + ex);
        } finally {

            try {
                getAmountOfUserActivities.close();
                resultSet.close();
                dbManager.commitAndClose(con);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }
        return activitiesAmount;
    }

    /**
     * get the statistic of user`s activities in specified language
     *
     * @param userId
     * @param activityId
     * @param language
     * @return
     */
    public List<UserActivityBean> getActivityBeansHistoryTranslation(Long userId, Long activityId, String language) {
        List<UserActivityBean> activityBeanList = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {

            con = dbManager.getConnection();
            stmt = con.prepareStatement(SQL__GET_USER_ACTIVITIES_HISTORY_TRANSLATION);
            stmt.setLong(1, userId);
            stmt.setLong(2, activityId);
            stmt.setString(3, language);
            rs = stmt.executeQuery();
            ActivityDAO.UserActivitiesBean mapper = new ActivityDAO.UserActivitiesBean();
            while (rs.next()) {
                activityBeanList.add(mapper.mapRow(rs));
            }
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            LOGGER.error("Error-->" + ex);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityBeanList;
    }

    public List<UserActivityBean> getActivityBeansHistory(Long userId, Long activityId) {
        List<UserActivityBean> activityBeanList = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {

            con = dbManager.getConnection();
            stmt = con.prepareStatement(SQL__GET_USER_ACTIVITIES_HISTORY);
            stmt.setLong(1, activityId);
            stmt.setLong(2, userId);
            rs = stmt.executeQuery();
            ActivityDAO.UserActivitiesBean mapper = new ActivityDAO.UserActivitiesBean();
            while (rs.next()) {
                activityBeanList.add(mapper.mapRow(rs));
            }
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            LOGGER.error("Error-->" + ex);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityBeanList;
    }

    /**
     * get the list of users activities in specified language
     *
     * @param start
     * @param total
     * @param userId
     * @param language
     * @return
     */
    public List<UserActivityBean> getActivityBeansTranslation(int start, int total, Long userId, String language) {
        List<UserActivityBean> activityBeanList = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {

            con = dbManager.getConnection();
            stmt = con.prepareStatement(SQL__GET_USER_ACTIVITIES_TRANSLATION);
            stmt.setLong(1, userId);
            stmt.setString(2, language);
            stmt.setInt(3, start);
            stmt.setInt(4, total);

            rs = stmt.executeQuery();
            ActivityDAO.UserActivitiesBean mapper = new ActivityDAO.UserActivitiesBean();
            while (rs.next()) {
                activityBeanList.add(mapper.mapRow(rs));
            }
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            LOGGER.error("Error-->" + ex);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityBeanList;
    }

    /**
     * get list of activities beans
     *
     * @param start
     * @param total
     * @param userId
     * @return
     */
    public List<UserActivityBean> getActivityBeans(int start, int total, Long userId) {
        List<UserActivityBean> activityBeanList = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {

            con = dbManager.getConnection();
            stmt = con.prepareStatement(SQL__GET_USER_ACTIVITIES);
            stmt.setLong(1, userId);
            stmt.setInt(2, start);
            stmt.setInt(3, total);
            rs = stmt.executeQuery();
            ActivityDAO.UserActivitiesBean mapper = new ActivityDAO.UserActivitiesBean();
            while (rs.next()) {
                activityBeanList.add(mapper.mapRow(rs));
            }
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            LOGGER.error("Error-->" + ex);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return activityBeanList;
    }

    /**
     * Extracts an activity information from the result set row.
     */
    private static class ActivityMapper implements EntityMapper<Activity> {

        @Override
        public Activity mapRow(ResultSet rs) {
            try {
                Activity activity = new Activity();
                activity.setId(rs.getLong(Fields.ACTIVITY_ID));
                activity.setName(rs.getString(Fields.ACTIVITY_NAME));
                Category category = new Category(rs.getString(Fields.CATEGORIES__CATEGORY_NAME));
                activity.setCategory(category);
                return activity;
            } catch (SQLException e) {
                LOGGER.trace(e);
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts an activity information with different parameters from the result set row.
     */
    private static class ActivityMapper2 implements EntityMapper<Activity> {

        @Override
        public Activity mapRow(ResultSet rs) {
            try {
                Activity activity = new Activity();
                activity.setId(rs.getLong(Fields.ACTIVITY_ID));
                Category category = new Category(rs.getString(Fields.CATEGORIES__CATEGORY_NAME));
                activity.setCategory(category);
                activity.setName(rs.getString(Fields.ACTIVITY_NAME));
                activity.setNameTranslation(rs.getString(Fields.ACTIVITY_TRANSLATION));

                return activity;
            } catch (SQLException e) {
                LOGGER.trace(e);
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts an activity translation from the result set row.
     */
    private static class ActivityMapperTranslation implements EntityMapper<Activity> {

        @Override
        public Activity mapRow(ResultSet rs) {
            try {
                Activity activity = new Activity();
                activity.setId(rs.getLong(1));
                activity.setName(rs.getString(2));
                Category category = new Category(rs.getString(3));
                activity.setCategory(category);
                return activity;
            } catch (SQLException e) {
                LOGGER.trace(e);
                throw new IllegalStateException(e);
            }
        }
    }


    /**
     * Extracts an users` activity translation from the result set row.
     */
    private static class ActivityUserMapperTranslation implements EntityMapper<Activity> {

        @Override
        public Activity mapRow(ResultSet rs) {
            try {
                Activity activity = new Activity();
                activity.setId(rs.getLong(1));
                activity.setName(rs.getString(2));
                Category category = new Category(rs.getString(3));
                activity.setCategory(category);
                activity.setAmountOfUsers(rs.getInt(4));
                return activity;
            } catch (SQLException e) {
                LOGGER.trace(e);
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts an activity information with amount of users from the result set row.
     */
    private static class ActivityUserMapper implements EntityMapper<Activity> {

        @Override
        public Activity mapRow(ResultSet rs) {
            try {
                Activity activity = new Activity();
                activity.setId(rs.getLong(Fields.ACTIVITY_ID));
                activity.setName(rs.getString(Fields.ACTIVITY_NAME));
                Category category = new Category(rs.getString(Fields.CATEGORIES__CATEGORY_NAME));
                activity.setCategory(category);
                activity.setAmountOfUsers(rs.getInt(4));
                return activity;
            } catch (SQLException e) {
                LOGGER.trace(e);
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts an activity information with time from the result set row.
     */
    private static class UsersActivity implements EntityMapper<UserActivityBean> {

        @Override
        public UserActivityBean mapRow(ResultSet rs) {
            try {
                UserActivityBean activity = new UserActivityBean();
                activity.setName(rs.getString(1));
                long millis = rs.getLong(2);
                long seconds = (millis / 1000) % 60;
                long minutes = ((millis - seconds) / 1000) / 60;
                long hours = minutes / 60;
                activity.setAllTime(hours);
                return activity;
            } catch (SQLException e) {
                LOGGER.trace(e);
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts an activities` categories information from the result set row.
     */
    private static class ActivityCategoryMapper implements EntityMapper<Activity> {

        @Override
        public Activity mapRow(ResultSet rs) {
            try {
                Activity activity = new Activity();
                activity.setId(rs.getLong(1));
                activity.setName(rs.getString(2));
                return activity;
            } catch (SQLException e) {
                LOGGER.trace(e);
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts an activity`s information and user`s information from the result set row.
     */
    private static class UserBeanMapper implements EntityMapper<UserBean> {

        @Override
        public UserBean mapRow(ResultSet rs) {
            try {
                UserBean bean = new UserBean();
                bean.setUserActivityId(rs.getLong(1));
                bean.setActivityId(rs.getLong(Fields.ACTIVITIES_USERS__ACTIVITIES_ID_ACTIVITIES));
                bean.setActivityName(rs.getString(3));
                bean.setUserId(rs.getInt(Fields.ACTIVITIES_USERS__USERS_ID_USERS));
                bean.setUserLogin(rs.getString(Fields.USER__MAIL));
                bean.setStart(rs.getTimestamp(Fields.ACTIVITIES_USERS__BEGINNING));
                bean.setEnd(rs.getTimestamp(Fields.ACTIVITIES_USERS__ENDING));
                bean.setStatus(rs.getInt(Fields.ACTIVITIES_USERS__STATUS));
                return bean;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts an user`s time from the result set row.
     */
    private static class UserActivitiesBean implements EntityMapper<UserActivityBean> {

        @Override
        public UserActivityBean mapRow(ResultSet rs) {
            try {
                UserActivityBean userActivitiesBean = new UserActivityBean();
                userActivitiesBean.setId(rs.getLong(1));
                userActivitiesBean.setName(rs.getString(2));
                if (rs.getTimestamp(3).toString().equals("1999-12-31 23:00:00.0")) {

                } else
                    userActivitiesBean.setBegin(rs.getTimestamp(3));
                if (rs.getTimestamp(4).toString().equals("1999-12-31 23:00:00.0")) {

                } else
                    userActivitiesBean.setEnd(rs.getTimestamp(4));
                return userActivitiesBean;
            } catch (SQLException e) {
                LOGGER.trace(e);
                throw new IllegalStateException(e);
            }
        }
    }

}
