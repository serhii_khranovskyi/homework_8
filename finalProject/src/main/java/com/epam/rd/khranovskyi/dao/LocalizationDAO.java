package com.epam.rd.khranovskyi.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Component
public class LocalizationDAO {

    private static final String SQL__GET_LANGUAGES_LIST =
            "Select name from language";

    @Autowired
    private DBManager dbManager;

    /**
     * get the language list from the language table
     *
     * @return list of strings
     */
    public ArrayList<String> getLanguagesList() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            con = dbManager.getConnection();

            pstmt = con.prepareStatement(SQL__GET_LANGUAGES_LIST);
            rs = pstmt.executeQuery();
            while (rs.next()) {

                arrayList.add(rs.getString("name"));
            }
            pstmt.close();
        } catch (SQLException ex) {
            dbManager.rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            dbManager.commitAndClose(con);
        }
        return arrayList;
    }
}
