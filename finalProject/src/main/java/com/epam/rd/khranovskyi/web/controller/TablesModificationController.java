package com.epam.rd.khranovskyi.web.controller;

import com.epam.rd.khranovskyi.service.*;
import com.epam.rd.khranovskyi.dao.bean.SessionModel;
import com.epam.rd.khranovskyi.web.exception.ClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Controller
public class TablesModificationController {
    private Model model;

    public TablesModificationController() {
        model = SessionModel.getModel();
    }

    @Autowired
    private ActivityUsersModifyService activityUsersModifyService;
    @Autowired
    private CategoryModifyService categoryModifyService;
    @Autowired
    private UsersModifyService usersModifyService;
    @Autowired
    private ActivitiesModifyService activitiesModifyService;
    @Autowired
    private ActivitiesLocalizationModifyService activitiesLocalizationModifyService;
    @Autowired
    private CategoryLocalizationModifyService categoryLocalizationModifyService;
    @Autowired
    private SortService sortService;
    @Autowired
    private UsersReportService usersReportService;
    @Autowired
    private UsersReportDetailService usersReportDetailService;

    @RequestMapping("/modify")
    public String user(@RequestParam String command, WebRequest webRequest) throws IOException, ClientException {
        if (command.equals("modifyActivityUser")) return activityUsersModifyService.execute(model, webRequest);
        if (command.equals("modifyCategories")) return categoryModifyService.execute(model, webRequest);
        if (command.equals("modifyUsers")) return usersModifyService.execute(model, webRequest);
        if (command.equals("modifyActivities")) return activitiesModifyService.execute(model, webRequest);
        if (command.equals("modifyActivitiesLocalization"))
            return activitiesLocalizationModifyService.execute(model, webRequest);
        if (command.equals("modifyCategoriesLocalization"))
            return categoryLocalizationModifyService.execute(model, webRequest);
        if (command.equals("sortActivityByName")) return sortService.execute(model, webRequest);
        if (command.equals("sortActivityByCategory")) return sortService.execute(model, webRequest);
        if (command.equals("sortActivityByAmount")) return sortService.execute(model, webRequest);
        if (command.equals("usersReport")) return usersReportService.execute(model, webRequest);
        if (command.equals("detail")) return usersReportDetailService.execute(model, webRequest);
        return null;
    }
}
