package com.epam.rd.khranovskyi.dao;

import com.epam.rd.khranovskyi.dao.bean.DbEntity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class DBManager {
    private static final Logger LOGGER = Logger.getLogger(DBManager.class);


    // //////////////////////////////////////////////////////////
    // singleton
    // //////////////////////////////////////////////////////////


    /**
     * Returns a DB connection from the Pool Connections. Before using this
     * method you must configure the Date Source and the Connections Pool in your
     * WEB_APP_ROOT/META-INF/context.xml file.
     *
     * @return A DB connection.
     */

    @Autowired
    private DbEntity dbEntity;
    public Connection getConnection() throws SQLException {
        Connection con = null;
        try {
            Class.forName(dbEntity.getDbDriver());
            con = DriverManager.getConnection(dbEntity.getDbURL() + dbEntity.getDbName()+ "?useUnicode=true&serverTimezone=UTC",
                    dbEntity.getDbUsername(),
                    dbEntity.getDbPassword());
        } catch (Exception ex) {
            LOGGER.error("Cannot obtain a connection from the pool", ex);
        }
        return con;
    }

    private DBManager() {
    }


    // //////////////////////////////////////////////////////////
    // DB util methods
    // //////////////////////////////////////////////////////////

    /**
     * Commits and close the given connection.
     *
     * @param con Connection to be committed and closed.
     */
    public void commitAndClose(Connection con) {
        try {
            //  con.commit();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            LOGGER.trace("Error in con.close -->" + ex);
        }
    }

    /**
     * Rollbacks and close the given connection.
     *
     * @param con Connection to be rollbacked and closed.
     */
    public void rollbackAndClose(Connection con) {
        try {
            // con.rollback();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

/**************** THIS METHOD IS NOT USED IN THE PROJECT *******/
    /**
     * Returns a DB connection. This method is just for a example how to use the
     * DriverManager to obtain a DB connection. It does not use a pool
     * connections and not used in this project. It is preferable to use
     * {@link #getConnection()} method instead.
     *
     * @return A DB connection.
     */
    public Connection getConnectionWithDriverManager() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/wb?serverTimezone=Europe/Moscow&useSSL=false&user=root&password=masterkey");
        connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        connection.setAutoCommit(false);
        return connection;
    }
}
