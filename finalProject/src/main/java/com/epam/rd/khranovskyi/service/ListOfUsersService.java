package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.UserDAO;
import com.epam.rd.khranovskyi.dao.entity.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class ListOfUsersService {
    private static final Logger LOG = Logger.getLogger(ListOfUsersService.class);

    @Autowired
    private UserDAO userDAO;

    private static class CompareById implements Comparator<User>, Serializable {
        private static final long serialVersionUID = -1573481565177573283L;

        public int compare(User bean1, User bean2) {
            if (bean1.getId() > bean2.getId())
                return 1;
            else return -1;
        }
    }

    private static Comparator<User> compareById = new ListOfUsersService.CompareById();

    public String execute(Model model, WebRequest request) throws IOException {

        //Get the list of users
        List<User> userBeanList = userDAO.getUsersBeans();
        LOG.trace("Found in DB: usersList --> " + userBeanList);

        Collections.sort(userBeanList, compareById);

        // put the list to request
        request.setAttribute("userList", userBeanList, RequestAttributes.SCOPE_REQUEST);
        LOG.trace("Set the request attribute: userList --> " + userBeanList);
        LOG.debug("Commands finished");
        return Path.PAGE__USERS;
    }

}
