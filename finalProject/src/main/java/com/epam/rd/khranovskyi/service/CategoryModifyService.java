package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.CategoryDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.entity.Category;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Service
public class CategoryModifyService {
    private static final Logger LOG = Logger.getLogger(CategoryModifyService.class);

    @Autowired
    private CategoryDAO categoryDAO;

    public String execute(Model model, WebRequest request) throws IOException {

        String combobox = request.getParameter("combobox");
        LOG.trace("Request parameter: combobox -->" + combobox);

        // Get  parameters from the request
        String category_id = request.getParameter("category_id");
        LOG.trace("Request parameter: activity_id --> " + category_id);

        String category_name = request.getParameter("category_name");
        LOG.trace("Request parameter: activity_name --> " + category_name);

        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        if (category_name == null) {
            errorMessage = "Parameters cannot be empty";
            request.setAttribute("errorMessage", errorMessage, RequestAttributes.SCOPE_REQUEST);
            LOG.error("errorMessage --> " + errorMessage);
            return forward;
        }
        //creating a category (Factory method)
        Category category = Category.createCategory(category_name);

        if (combobox.equals("0")) {
            //insert category
            categoryDAO.insertCategory(category);
        }
        if (combobox.equals("1")) {
            if (!category_id.isEmpty()) {
                category.setId(Long.parseLong(category_id));
                //update category
                categoryDAO.updateCategory(category);
            } else {
                return forward;
            }
        }
        if (combobox.equals("2")) {
            if (!category_id.isEmpty()) {
                category.setId(Long.parseLong(category_id));
                //delete category
                categoryDAO.deleteCategory(category);
            } else {
                return forward;
            }

        }


        return "redirect:" + Path.COMMAND__CATEGORIES;
    }
}
