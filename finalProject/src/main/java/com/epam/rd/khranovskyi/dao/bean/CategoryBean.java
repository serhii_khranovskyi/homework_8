package com.epam.rd.khranovskyi.dao.bean;

import com.epam.rd.khranovskyi.dao.entity.Entity;

public class CategoryBean extends Entity {
    private String name;

    private String translation;

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CategoryBean{" +
                "name='" + name + '\'' +
                '}';
    }
}
