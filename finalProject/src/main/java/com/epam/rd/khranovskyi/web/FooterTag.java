package com.epam.rd.khranovskyi.web;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class FooterTag extends SimpleTagSupport {
    StringWriter stringWriter = new StringWriter();
    public void doTag()

            throws JspException, IOException {
        getJspBody().invoke(stringWriter);
        getJspContext().getOut().println(stringWriter.toString());
    }
}
