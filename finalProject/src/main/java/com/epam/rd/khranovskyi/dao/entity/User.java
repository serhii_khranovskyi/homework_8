package com.epam.rd.khranovskyi.dao.entity;

import java.util.ArrayList;
import java.util.Objects;

public class User extends Entity {
    private String mail;
    private String first_name;
    private String last_name;
    private String password;
    private Language language;
    private Role role;
    private ArrayList<Activity> activities;
    private int activitiesCount;
    private long allActivitiesTime;


    public static User createUser(String mail, String first_name, String last_name, String password, Role role, Language language, ArrayList<Activity> activities) {
        User user = new User();
        user.setMail(mail);
        user.setFirst_name(first_name);
        user.setLast_name(last_name);
        user.setPassword(password);
        user.setRole(role);
        user.setLanguage(language);
        user.setActivities(activities);
        return user;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getActivitiesCount() {
        return activitiesCount;
    }

    public void setActivitiesCount(int activitiesCount) {
        this.activitiesCount = activitiesCount;
    }

    public long getAllActivitiesTime() {
        return allActivitiesTime;
    }

    public void setAllActivitiesTime(long allActivitiesTime) {
        this.allActivitiesTime = allActivitiesTime;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public ArrayList<Activity> getActivities() {
        return activities;
    }

    public void setActivities(ArrayList<Activity> activities) {
        this.activities = activities;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append("User [ mail = ")
                .append(mail)
                .append("; first_name = ")
                .append(first_name)
                .append("; last_name = ")
                .append(last_name)
                .append("; password = ")
                .append(password)
                .append("]")
               .toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj.getClass() != this.getClass()) {
            return false;
        }

        final User other = (User) obj;
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }

        if (this.getId() != other.getId()) {
            return false;
        }

        return true;
    }
}
