package com.epam.rd.khranovskyi.web.controller;

import com.epam.rd.khranovskyi.service.*;
import com.epam.rd.khranovskyi.dao.bean.SessionModel;
import com.epam.rd.khranovskyi.web.exception.ClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Controller
public class UserController {
    private Model model;

    @Autowired
    private EditProfileService editProfileService;

    @Autowired
    private ListCategoriesService listCategoriesService;
    @Autowired
    private UsersActivitiesService usersActivitiesService;
    @Autowired
    private UpdateUserActivityService updateUserActivityService;
    @Autowired
    private SelectedCategoryService selectedCategoryService;
    @Autowired
    private DeleteUserActivityService deleteUserActivityService;
    @Autowired
    private ProfileService profileService;
    @Autowired
    private SelectedActivityService selectedActivityService;
    @Autowired
    private HistoryUsersActivityService  historyUsersActivityService;


    public UserController() {
        model = SessionModel.getModel();
    }

    @RequestMapping("/user")
    public String user(@RequestParam String command, WebRequest webRequest) throws IOException, ClientException {
        if (command.equals("users_main_page")) return listCategoriesService.execute(model, webRequest);
        if (command.equals("myActivitiesCommand")) return usersActivitiesService.execute(model, webRequest);
        if (command.equals("updateUserActivity")) return updateUserActivityService.execute(model, webRequest);
        if (command.equals("selectedCategory")) return selectedCategoryService.execute(model, webRequest);
        if (command.equals("deleteUserActivity")) return deleteUserActivityService.execute(model, webRequest);
        if (command.equals("profile")) return profileService.execute(model, webRequest);
        if (command.equals("editProfile")) return editProfileService.execute(model, webRequest);
        if (command.equals("selectedActivity")) return selectedActivityService.execute(model, webRequest);
        if (command.equals("historyUserActivity"))
            return historyUsersActivityService.execute(model, webRequest);
        return null;
    }
}
