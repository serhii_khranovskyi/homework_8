package com.epam.rd.khranovskyi.dao.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name= "users")
public class UserEntity {
    @Id
    @GeneratedValue
    @Column(name = "id_users")
    private Long id;

    private String first_name;

    private String last_name;

    private String mail;

    private String password;

    private int role_id_role;

    private int language_id_language;
}
