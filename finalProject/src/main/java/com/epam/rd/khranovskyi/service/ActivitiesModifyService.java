package com.epam.rd.khranovskyi.service;

import com.epam.rd.khranovskyi.dao.ActivityDAO;
import com.epam.rd.khranovskyi.dao.LocalizationDAO;
import com.epam.rd.khranovskyi.dao.Path;
import com.epam.rd.khranovskyi.dao.entity.Activity;
import com.epam.rd.khranovskyi.dao.entity.Category;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.util.List;

@Service
public class ActivitiesModifyService {
    private static final Logger LOG = Logger.getLogger(ActivitiesModifyService.class);

    @Autowired
    private ActivityDAO activityDAO;
    @Autowired
    private LocalizationDAO localizationDAO;

    public String execute(Model model, WebRequest request) throws IOException {
        String combobox = request.getParameter("combobox");
        LOG.trace("Request parameter: combobox -->" + combobox);

        String activity_id = request.getParameter("activity_id");
        LOG.trace("Request parameter: activity_id --> " + activity_id);

        String activity_name = request.getParameter("activity_name");
        LOG.trace("Request parameter: activity_name --> " + activity_name);

        String activity_name_translation = request.getParameter("activity_name_translation");
        LOG.trace("Request parameter: activity_name_translation --> " + activity_name_translation);

        String category = request.getParameter("category");
        LOG.trace("Request parameter: category --> " + category);


        String errorMessage = null;
        String forward = Path.PAGE__ERROR_PAGE;

        if (activity_name == null || category == null) {
            errorMessage = "Parameters cannot be empty";
            request.setAttribute("errorMessage", errorMessage, RequestAttributes.SCOPE_REQUEST);
            LOG.error("errorMessage --> " + errorMessage);
            return forward;
        }
        Category objCategory = Category.createCategory(category);
        Activity activity = Activity.createActivity(activity_name, objCategory);
        //Insert Activity without translation
        if (combobox.equals("0")) {
            activityDAO.insertActivityWithoutTranslation(activity);
        }
        //Update activity without translation
        if (combobox.equals("1")) {
            if (!activity_id.isEmpty()) {
                activity.setId(Long.parseLong(activity_id));
                activityDAO.updateActivity(activity);
            } else {
                return forward;
            }
        }
        //Delete activity
        if (combobox.equals("2")) {
            if (!activity_id.isEmpty()) {
                activity.setId(Long.parseLong(activity_id));
                activityDAO.deleteActivity(activity);
            } else {
                return forward;
            }

        }

        //get languages for radio buttons
        List<String> languagesList = localizationDAO.getLanguagesList();
        LOG.trace("Found in DB: list of languages -->" + languagesList);
        request.setAttribute("languagesList", languagesList, RequestAttributes.SCOPE_REQUEST);


        return "redirect:" + Path.COMMAND__ACTIVITIES;
    }
}
