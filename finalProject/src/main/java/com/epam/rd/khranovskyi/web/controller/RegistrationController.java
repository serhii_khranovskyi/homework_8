package com.epam.rd.khranovskyi.web.controller;


import com.epam.rd.khranovskyi.dao.bean.SessionModel;
import com.epam.rd.khranovskyi.service.LoginService;
import com.epam.rd.khranovskyi.service.LogoutService;
import com.epam.rd.khranovskyi.service.RegistrationService;
import com.epam.rd.khranovskyi.web.exception.ClientException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;

@Controller
public class RegistrationController {
    private static final Logger LOG = Logger.getLogger(RegistrationController.class);
    private Model model;

    @Autowired
    private LoginService loginService;
    @Autowired
    private LogoutService logoutService;
    @Autowired
    private RegistrationService registrationService;

    public RegistrationController() {
        if (SessionModel.getModel() == null)
            model = SessionModel.createModel();
        else model = SessionModel.getModel();
    }

    @PostMapping("/register")
    public String register(WebRequest request) throws IOException, ClientException {
        LOG.trace("registration!");
        return registrationService.execute(model, request);
    }

    @PostMapping("/login")
    public String login(WebRequest request) throws IOException, ClientException {
        return loginService.execute(model, request);
    }

    @RequestMapping("/logout")
    public String logout(WebRequest request) throws IOException, ClientException {
        return logoutService.execute(model, request);

    }

    @RequestMapping("/reg")
    public String showRegistration() {
        return "registration";
    }
}
