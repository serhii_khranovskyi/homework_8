<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="List users" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<%@ include file="/WEB-INF/jspf/directive/register_style.jspf" %>


<body>
<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">
    <ul>
        <li style="margin: 30px;padding-left: 350px"><a href="/accounting_time/table?command=listUsers"><fmt:message
                key="users_activities_jsp.head.link.activities_user"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/table?command=activitiesTable"><fmt:message
                key="list_users_jsp.table.header.activity"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/table?command=categoriesTable"> <fmt:message
                key="users_activities_jsp.head.link.categories"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/modify?command=usersReport"><fmt:message
                key="users_jsp.head.link.report"/></a></li>
        <li style="margin: 30px;padding-right: 350px"><a href="/accounting_time/logout"><fmt:message
                key="list_users_jsp.table.header.logout"/></a></li>
        <li class="helper"></li>
    </ul>
</nav>

<table id="main-container">

    <tr>
        <td class="modifyUsers">

            <form id="modify_user" action="modify" method="post">

                <input type="hidden" name="command" value="modifyUsers"/>

                <fieldset>
                    <legend>
                        <fmt:message key="users_jsp.label.content.modifyUserInfo"/>
                    </legend>
                    <select name="combobox">
                        <option value="0">Insert</option>
                        <option value="1">Update</option>
                        <option value="2">Delete</option>
                    </select>

                    <br/> <br/>

                    <legend>

                        <fmt:message key="users_jsp.label.content.firstname"/>
                        <input type="text" name="first_name"/>
                        <br/> <br/>


                        <fmt:message key="users_jsp.label.content.lastname"/>

                        <input type="text" name="last_name"/>
                        <br/> <br/>


                        <fmt:message key="users_jsp.label.content.mail"/>

                        <input type="text" name="mail"/>
                        <br/> <br/>


                        <fmt:message key="users_jsp.label.content.password"/>

                        <input type="text" name="password"/>
                        <br/> <br/>


                        <fmt:message key="users_jsp.label.content.language"/>

                        <div class="language">
                            <input type="radio" name="selectEnglish">

                            <fmt:message key="users_jsp.label.content.role.selectEnglish"/>


                            <input type="radio" name="selectRussian">

                            <fmt:message key="users_jsp.label.content.role.selectRussian"/>

                        </div>
                        <br/> <br/>


                        <fmt:message key="users_jsp.label.content.role"/>

                        <div class="role">
                            <input type="radio" name="selectUser">

                            <fmt:message key="users_jsp.label.content.role.selectUser"/>


                            <input type="radio" name="selectAdmin">

                            <fmt:message key="users_jsp.label.content.role.selectAdmin"/>

                        </div>
                    </legend>
                </fieldset>
                <br/> <br/>

                <input type="submit" value='<fmt:message key="list_users_jsp.button.apply"/>'>
            </form>


        </td>
    </tr>

    <tr>
        <td class="content">

            <c:choose>
                <c:when test="${fn:length(userList) == 0}">No such table</c:when>

                <c:otherwise>
                    <table id="list_user_table">
                        <thead>
                        <tr>
                            <td><fmt:message key="users_jsp.table.header.firstName"/></td>
                            <td><fmt:message key="users_jsp.table.header.lastName"/></td>
                            <td><fmt:message key="users_jsp.table.header.mail"/></td>
                            <td><fmt:message key="users_jsp.table.header.password"/></td>
                            <td><fmt:message key="users_jsp.table.header.role"/></td>
                            <td><fmt:message key="users_jsp.table.header.language"/></td>
                        </tr>
                        </thead>

                        <c:forEach var="bean" items="${userList}">

                            <tr>
                                <td>${bean.first_name}</td>
                                <td>${bean.last_name}</td>
                                <td>${bean.mail}</td>
                                <td>${bean.password}</td>
                                <td>${bean.role}</td>
                                <td>${bean.language}</td>
                            </tr>

                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
</table>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>