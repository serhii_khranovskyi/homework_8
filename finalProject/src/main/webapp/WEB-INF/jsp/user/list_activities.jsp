<%@ page language="java" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Categories list" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/register_style.jspf" %>


<body>
<nav class="navbar navbar-inverse" style=" background-color: #92D3E8;">
    <ul>
        <li style="margin: 30px;padding-left: 350px"><a href="/accounting_time/user?command=profile"><fmt:message
                key="users_activities_jsp.head.link.profile"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/user?command=myActivitiesCommand"> <fmt:message
                key="users_main_page_jsp.table.button.my_activities"/></a></li>
        <li style="margin: 30px;"><a href="/accounting_time/user?command=users_main_page"><fmt:message
                key="users_activities_jsp.head.link.categories"/></a></li>
        <li style="margin: 30px;padding-right: 350px"><a href="/accounting_time/logout"><fmt:message
                key="list_users_jsp.table.header.logout"/></a></li>
        <li class="helper"></li>
    </ul>
</nav>
<table style="width: 90%; margin: auto">

    <tr>

        <c:choose>
            <c:when test="${fn:length(activityList) == 0}">No such table</c:when>

            <c:otherwise>

                <c:forEach var="bean" items="${activityList}">
                    <form action="user" method="post">
                        <p>${bean.name}</p>
                        <input type="hidden" name="command" value="selectedActivity"/>
                        <input type="hidden" name="activityId" value="${bean.id}">
                        <input type="submit" value="<fmt:message key="list_activities_jsp.button.add"/>">
                    </form>
                </c:forEach>

            </c:otherwise>
        </c:choose>


    </tr>
</table>
<table style="width: 90%; margin: auto">
    <tr>
        <c:if test="${currentPage != 1}">

            <form action="user" method="post">
                <input type="hidden" name="command" value="users_main_page">
                <input type="hidden" name="page" value="${currentPage - 1}">
                <input type="submit" value="Previous">
            </form>

        </c:if>
        <c:forEach begin="1" end="${noOfPages}" var="i">
            <c:choose>
                <c:when test="${currentPage eq i}">
                    <input type="submit" value="${i}">
                </c:when>
                <c:otherwise>
                    <form action="user" method="post">
                        <input type="hidden" name="command" value="users_main_page">
                        <input type="hidden" name="page" value="${i}">
                        <input type="submit" value="${i}">
                    </form>
                </c:otherwise>
            </c:choose>
        </c:forEach>

        <c:if test="${currentPage lt noOfPages}">
            <form action="user" method="post">
                <input type="hidden" name="command" value="users_main_page">
                <input type="hidden" name="page" value="${currentPage + 1}">
                <input type="submit" value="Next">
            </form>


        </c:if>
    </tr>
</table>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
