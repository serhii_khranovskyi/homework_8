<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>


<html>

<c:set var="title" value="Login"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="jspf/directive/login_style.jspf" %>

<body>


<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>


<form method="get" action="language">
    <input type="hidden" name="command" value="language"/>
    <input type="hidden" name="view">
    <select name="language" onchange="submit()">
        <option value="ru" ${language == 'ru' ? 'selected' : ''}><fmt:message
                key="language_menu_jspf.language.russian"/></option>
        <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
    </select>
</form>


<div class="container">
    <div class="row">

        <div class="col-md-offset-3 col-md-6">
            <form class="form-horizontal" id="login_form" action="/accounting_time/login" method="post">
                <input type="hidden" name="command" value="login"/>
                <span class="heading"><fmt:message key="login_jsp.label.authorization"/></span>
                <div class="form-group">
                    <input type="email" name="login" class="form-control" id="inputEmail"
                           placeholder="<fmt:message key="login_jsp.label.login"/>">
                    <i class="fa fa-user"></i>
                </div>
                <div class="form-group help">
                    <input type="password" name="password" class="form-control" id="inputPassword"
                           placeholder="<fmt:message key="login_jsp.label.password"/>">
                    <i class="fa fa-lock"></i>
                </div>

                <button type="submit" style="text-align: center;" class="btn btn-primary"><fmt:message
                        key="login_jsp.button.login"/></button>
            </form>

            <form class="form-horizontal" action="/accounting_time/reg">
                <button type="submit" style="text-align: center;" class="btn btn-primary d-block mx-auto"><fmt:message
                        key="login_jsp.href.registration"/></button>
            </form>
        </div>

    </div>
</div>
</body>
</html>
