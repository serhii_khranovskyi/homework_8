<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Registration"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@include file="jspf/language_menu.jspf" %>
<%@include file="jspf/directive/register_style.jspf" %>
<div class="container">
    <div class="row main-form">
        <form id="registration_form" action="/accounting_time/register" method="post">
            <input type="hidden" name="command" value="registration"/>
            <div class="form-group">
                <label for="name" class="cols-sm-2 control-label"><fmt:message
                        key="registration.label.content.firstname"/></label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="first_name" id="name"
                               placeholder="<fmt:message key="registration.label.content.firstname"/>"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="cols-sm-2 control-label"><fmt:message
                        key="registration.label.content.lastname"/></label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="last_name" id="username"
                               placeholder="<fmt:message key="registration.label.content.lastname"/>"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="cols-sm-2 control-label"><fmt:message
                        key="registration.label.content.mail"/></label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="mail" id="email"
                               placeholder="<fmt:message key="registration.label.content.mail"/>"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="cols-sm-2 control-label"><fmt:message
                        key="registration.label.content.password"/></label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                        <input type="password" class="form-control" name="password" id="password"
                               placeholder="<fmt:message key="registration.label.content.password"/>"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="confirm" class="cols-sm-2 control-label"><fmt:message
                        key="registration.label.content.rewritePassword"/></label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                        <input type="password" class="form-control" name="rewrite_password" id="confirm"
                               placeholder="<fmt:message key="registration.label.content.rewritePassword"/>"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <%--                <input type="hidden" name="command" value="registration"/>--%>
                <input type="submit" id="button" class="btn btn-primary btn-lg btn-block login-button"
                       value="<fmt:message key="registration.button.apply"/>">
            </div>
        </form>
    </div>
</div>
</body>
</html>
